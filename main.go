package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"shortener/ssdb"
)

const (
	base62_power uint64 = 62
	host                = "localhost"
	port         uint16 = 8080
)

var (
	BASE62_ALPHABET = [base62_power]rune{
		'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
		'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
		'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
	}
	base62Lookup map[rune]int
)

func base62_init() {
	base62Lookup = make(map[rune]int, base62_power)
	for k, v := range BASE62_ALPHABET {
		base62Lookup[v] = k
	}
}

type Factory struct {
	chars  [base62_power]rune
	lookup map[rune]int
}

func New(chars [base62_power]rune) *Factory {
	if uint64(len(chars)) != base62_power {
		fmt.Println("Length must be equal to 62")
		os.Exit(1)
	}
	lookup := make(map[rune]int, base62_power)
	for i, c := range chars {
		lookup[c] = i
	}
	return &Factory{
		chars:  chars,
		lookup: lookup,
	}
}

func (f *Factory) Encode(in uint64) string {
	return encode(f.chars, in)
}

func (f *Factory) Decode(s string) uint64 {
	return decode(f.lookup, s)
}

func Encode(in uint64) string {
	return encode(BASE62_ALPHABET, in)
}

func encode(chars [base62_power]rune, in uint64) string {
	if in < 1 {
		return ""
	}
	i, tmp := 0, in
	for tmp > 0 {
		i++
		if tmp == base62_power {
			break
		}
		tmp /= base62_power
	}

	out := make([]rune, i)
	for in > 0 {
		i--
		if in%base62_power == 0 {
			out[i] = chars[base62_power-1]
			if in == base62_power {
				break
			}
		} else {
			out[i] = chars[in%base62_power-1]
		}
		in /= base62_power
	}
	return string(out)
}

func Decode(s string) uint64 {
	return decode(base62Lookup, s)
}

func decode(lookup map[rune]int, s string) uint64 {
	var sum uint64
	for i, v := range s {
		val := uint64(lookup[v] + 1)
		if i > 0 {
			val = val % base62_power
		}
		sum = sum*base62_power + val
	}
	return sum
}

func main() {
	full_host := fmt.Sprintf("%s:%d", host, port)

	http.HandleFunc("/a", ProcA)
	http.HandleFunc("/a/", ProcA)
	http.HandleFunc("/s", ProcS)
	http.HandleFunc("/s/", ProcS)
	fmt.Println("Connecting to", full_host)

	// Running server at localhost:8080 (but also 127.0.0.1)
	log.Fatal(http.ListenAndServe(full_host, nil))
}

// Generating SSDB Key by URL as input
func ProcA(rw http.ResponseWriter, r *http.Request) {
	url := r.URL.Query().Get("url")

	// Checking if URL is Empty
	if len(url) < 1 {
		fmt.Println("Empty URL")
		return
	}

	// Output key into HTML document
	s := GetLink(url)
	rw.WriteHeader(http.StatusOK)
	rw.Write([]byte(s))

	// ssdb start
	// To use SSDB run './ssdb-server ssdb.conf' in ssdb-master directory.
	ssdb_ip := "127.0.0.1"
	ssdb_port := 8888
	db, err := ssdb.Connect(ssdb_ip, ssdb_port)

	if err != nil {
		fmt.Println("ProcA: SSDB: cannot init: ", err)
		return
	}

	var val interface{}
	db.Set(s, url)

	val, err = db.Get(s)
	if err != nil {
		fmt.Println("Cannot get value: ", err)
		return
	}
	fmt.Println("val = ", val)
	// ssdb end
}

func GetLink(url string) string {
	base62_init()
	s := url
	length := len(s)

	var q [8]uint
	var sum, th, cnt int
	cnt = 7
	th1 := 1

	// This loop calculates checksum
	for i := (length - 1); i >= 0; i-- {
		if (th % length) > ((th + 8) % length) {
			stl := (th1*length - th) % length
			inv := (8 - stl)
			f := int(s[i]&((1<<stl)-1)) & 0xFF
			sum += f

			th1++
			q[cnt] = uint(sum)
			cnt--
			sum = 0
			g := int((s[i] >> inv)) & 0xFF
			sum += g
		} else {
			sum += int(s[i])
		}
		th += 8
	}

	var result62 [8]uint
	for j := 0; j < len(result62); j++ {
		rs := q[j]
		r_sum := ((rs/uint(base62_power) + (rs % uint(base62_power))) % uint(base62_power))
		result62[j] = r_sum
	}
	fmt.Println(result62)
	var b [8]byte
	for qw := range b {
		b[qw] = byte(BASE62_ALPHABET[result62[qw]])
	}

	// Getting key as string
	output := fmt.Sprintf("%c%c%c%c%c%c%c%c",
		b[0], b[1], b[2], b[3],
		b[4], b[5], b[6], b[7])

	return output
}

// Getting URL from key as input, that generated from SSDB
func ProcS(rw http.ResponseWriter, r *http.Request) {
	var urlstr string
	fmt.Println(rw)
	path := r.URL.Path
	code, err := fmt.Sscanf(path, "/s/%s", &urlstr)
	fmt.Println(urlstr)
	if code <= 0 || err != nil {
		fmt.Printf("Length of url: %d\n", code)
		fmt.Println("Cause: ", err)
		return
	}

	// ssdb start
	ssdb_ip := "127.0.0.1"
	ssdb_port := 8888
	db, err := ssdb.Connect(ssdb_ip, ssdb_port)
	if err != nil {
		fmt.Println("ProcS: SSDB: cannot init: ", err)
		os.Exit(1)
	}

	var val interface{}

	// Getting actual URL by knowing key
	val, err = db.Get(urlstr)
	if val == nil || err != nil {
		fmt.Println(val, err)
		return
	}

	// Getting URL from key as string
	url := fmt.Sprintf("%s", val)

	http.Redirect(rw, r, url, http.StatusFound)
	// ssdb end
}
